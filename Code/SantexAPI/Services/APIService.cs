﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using SantexAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using SantexAPI.DAL;

namespace SantexAPI.Services
{
    public class APIService
    {
        public HttpClient _client;

        public APIService()
        {
            _client = new HttpClient();
        }

        public async Task<APIData> GetDataFromAPI(string code)
        {
            APIData apiData = null;
            List<Models.TeamInfo> teamInfoList = new List<TeamInfo>();
            ResultMessage rm = new ResultMessage(null, null);
            ImportClass importClass = new ImportClass();
            try
            {
                // Update port # in the following line.

                string url = string.Format(ConfigurationManager.AppSettings.Get("APIUrl") + "/competitions/{0}/teams", code);
                _client = GetHttpClient(url);

                string response = await _client.GetStringAsync(_client.BaseAddress);
                importClass = JsonConvert.DeserializeObject<ImportClass>(response);

                //needs to add per each team -> all players the team has
               
                Models.TeamInfo teamInfo;
                foreach (Models.Team team in importClass.teams)
                {
                    url = ConfigurationManager.AppSettings.Get("APIUrl") + string.Concat("/teams/", team.id);
                    _client = GetHttpClient(url);
                    response = await _client.GetStringAsync(_client.BaseAddress);
                    teamInfo = JsonConvert.DeserializeObject<Models.TeamInfo>(response);
                    teamInfoList.Add(teamInfo);
                }
               
            }
            catch(Exception ex)
            {
                if (ex.Message.LastIndexOf("429") != -1)
                    rm.Code = 429;
               
                rm.Message = ex.Message;
            }
            finally
            {
                apiData = new APIData(importClass, teamInfoList, rm);
                if (rm.Code == 429)
                {
                    if (importClass.teams.Count != teamInfoList.Count)
                        rm.Message += " Players for teams retrieved: " + teamInfoList.Count + ", Players for teams found: " + importClass.teams.Count;
                }
                else
                {
                    //importClass.competition = null;
                    rm.Code = 404;
                    rm.Message = "League: " + code + " not found. " + rm.Message;
                }
              
            }
            return apiData;
        }


        private HttpClient GetHttpClient(string url)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(url);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            _client.DefaultRequestHeaders.Add("X-Auth-Token", ConfigurationManager.AppSettings.Get("AppTokenKey"));
            return _client;
        }
    }
}