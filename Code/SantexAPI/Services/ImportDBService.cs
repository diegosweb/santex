﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SantexAPI.Models;
using SantexAPI.DAL;


namespace SantexAPI.Services
{
    public class ImportDBService
    {
        private SantexDBConnection _SantexDBConnection;
        private List<DAL.Position> listAllPositions;
        private List<DAL.Country> listAllCountries;
        private List<DAL.Area> listAllAreas;
        private List<DAL.Player> listAllPlayers;
        private List<DAL.Competition> listAllCompetitions;
        public string Code { get; set; }

        public ImportDBService(string code)
        {
            _SantexDBConnection = new SantexDBConnection();
            this.Code = code;
            listAllPositions = new List<Position>();
            listAllCountries = new List<Country>();
            listAllAreas = new List<DAL.Area>();
            listAllCompetitions = new List<DAL.Competition>();
            listAllPlayers = new List<DAL.Player>();


        }


        public bool CheckLeagueExists()
        {
            DAL.Competition league = _SantexDBConnection.Competition.FirstOrDefault<DAL.Competition>(x => x.Code == this.Code);
            if (league != null)
                return true;
            else
                return false;
        }

        public ResultMessage ImportIntoDB(ImportClass importClass, List<TeamInfo> teamInfoList, ResultMessage rm)
        {
            

            if (teamInfoList.Count == 0 && rm.Message != null)
            {
                rm.Code = 504;
                rm.Message += " .Issue with Football API was found. Team information was not retrieved. ";
                return rm;
            }
            try
            {
                foreach (Models.Team team in importClass.teams)
                {
                    TeamInfo teamInfo = teamInfoList.Where<TeamInfo>(x => x.id == team.id).FirstOrDefault();
                    if (teamInfo != null)
                    {
                        SaveTeamInfoData(importClass.competition.id,  teamInfo);
                    }
                }

                _SantexDBConnection.SaveChanges();

                if (string.IsNullOrEmpty(rm.Message))
                    return new ResultMessage(200, "OK");
                else
                    return new ResultMessage(rm.Code, "Import process worked correctly, however not all players were returned from Football API. " + rm.Message);
            }
            catch(Exception ex)
            {
                return new ResultMessage(504, "Error: " + ex.InnerException.InnerException.Message);
            }
          
        }

        private bool SaveTeamInfoData(int competitionID, TeamInfo teamInfo)
        {
            List<DAL.TeamCompetition> listTeamCompetitions = new List<TeamCompetition>();

            //first save Area in Database
            #region Area
            DAL.Area dbArea = _SantexDBConnection.Area.FirstOrDefault<DAL.Area>(x => x.ID == teamInfo.area.id);
            if (dbArea == null)
            {
                if (listAllAreas.FirstOrDefault<DAL.Area>(x => x.ID == teamInfo.area.id) == null)
                {
                    dbArea = new DAL.Area();
                    dbArea.ID = teamInfo.area.id;
                    dbArea.Name = teamInfo.area.name;
                    listAllAreas.Add(dbArea);
                    _SantexDBConnection.Area.Add(dbArea);
                }
            }
            #endregion

            //then save Team in Database
            #region Team
            DAL.Team dbTeam = _SantexDBConnection.Team.FirstOrDefault<DAL.Team>(x => x.ID == teamInfo.id);
            if (dbTeam == null)
            {
                dbTeam = new DAL.Team();
                dbTeam.Email = teamInfo.email;
                dbTeam.ID = teamInfo.id;
                dbTeam.Name = teamInfo.name;
                dbTeam.ShortName = teamInfo.shortName;
                dbTeam.TLA = teamInfo.tla;
                dbTeam.Area = dbArea;
               _SantexDBConnection.Team.Add(dbTeam);
            }
            #endregion

            #region TeamCompetition

            Models.Competition competition = teamInfo.activeCompetitions.FirstOrDefault<Models.Competition>(x => x.id == competitionID);
            //competition should not be null, but as the API has inconsistencies most of times it's null.
            if (competition != null)
            {
                //save first competitions in Database
                #region Competition
                DAL.Competition dbCompetition = _SantexDBConnection.Competition.FirstOrDefault<DAL.Competition>(x => x.ID == competition.id);
                if (dbCompetition == null)
                {
                    if (listAllCompetitions.FirstOrDefault<DAL.Competition>(x => x.ID == competition.id) == null)
                    {
                        dbCompetition = new DAL.Competition();
                        dbCompetition.ID = competition.id;
                        dbCompetition.Area = listAllAreas.FirstOrDefault<DAL.Area>(x => x.ID == competition.area.id);
                        dbCompetition.Name = competition.name;
                        dbCompetition.Code = competition.code;

                        listAllCompetitions.Add(dbCompetition);
                        _SantexDBConnection.Competition.Add(dbCompetition);
                    }
                }


                DAL.TeamCompetition dbTeamCompetition = _SantexDBConnection.TeamCompetition.FirstOrDefault<DAL.TeamCompetition>(x => x.TeamID == teamInfo.id && x.CompetitionID == competition.id);
                if (dbTeamCompetition == null)
                {
                    dbTeamCompetition = new DAL.TeamCompetition();
                    dbTeamCompetition.CompetitionID = competition.id;
                    dbTeamCompetition.IsActive = true;
                    dbTeamCompetition.TeamID = teamInfo.id;
                    _SantexDBConnection.TeamCompetition.Add(dbTeamCompetition);
                }
                #endregion
            }

            #endregion

            #region AllPlayers
            foreach (Models.Player player in teamInfo.squad)
            {
                #region Position
                DAL.Position dbPosition = null;
                if (!string.IsNullOrEmpty(player.position))
                { 
                   dbPosition = _SantexDBConnection.Position.FirstOrDefault<Position>(x => x.Name == player.position);
                    if (dbPosition == null)
                    {
                        if (listAllPositions.FirstOrDefault<Position>(x => x.Name == player.position) == null)
                        {
                        
                            dbPosition = new DAL.Position();
                            dbPosition.Name = player.position;
                            listAllPositions.Add(dbPosition);
                            _SantexDBConnection.Position.Add(dbPosition);
                        }
                    }
                }
                #endregion

                #region Country
                DAL.Country dbCountry = null;
                if (!string.IsNullOrEmpty(player.nationality))
                { 
                    dbCountry = _SantexDBConnection.Country.FirstOrDefault<Country>(x => x.Name == player.nationality);
                    if (dbCountry == null)
                    {
                        if (listAllCountries.FirstOrDefault<Country>(x => x.Name == player.nationality) == null)
                        {
                            dbCountry = new DAL.Country();
                            dbCountry.Name = player.nationality;
                            listAllCountries.Add(dbCountry);
                            _SantexDBConnection.Country.Add(dbCountry);
                        }

                    }
                }
                #endregion

                #region Player

                if (player.id == 96)
                {
                    string name = player.name;
                    string team = teamInfo.name;
                }
                //known issue: one player should not belong to two different teams, but I checked that this happened so players may be duplicated
                //that's why I update it's team ID if it exists.

                DAL.Player dbPlayer = _SantexDBConnection.Player.FirstOrDefault<DAL.Player>(x => x.ID == player.id);
                if (dbPlayer == null)
                {
                    if (listAllPlayers.FirstOrDefault<DAL.Player>(x => x.ID == player.id) == null)
                    {
                        dbPlayer = new DAL.Player();
                        dbPlayer.ID = Convert.ToInt32(player.id);
                        dbPlayer.DateOfBirth = Convert.ToDateTime(player.dateOfBirth);
                        dbPlayer.Name = player.name;

                        if (!string.IsNullOrEmpty(player.nationality))
                            dbPlayer.Country = dbCountry;
                        else
                            dbPlayer.Country = null;


                        if (!string.IsNullOrEmpty(player.position))
                            dbPlayer.Position = dbPosition;
                        else
                            dbPlayer.Position = null;


                        dbPlayer.TeamID = teamInfo.id;
                        dbPlayer.Team = dbTeam;
                        listAllPlayers.Add(dbPlayer);
                        _SantexDBConnection.Player.Add(dbPlayer);
                    }
                }
                else
                {
                    dbPlayer.TeamID = teamInfo.id;
                    _SantexDBConnection.Player.Attach(dbPlayer);
                }

                #endregion
            }
            #endregion


          
            return true;
        }

        

       

    }
}