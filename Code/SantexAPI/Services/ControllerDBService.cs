﻿using SantexAPI.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Infrastructure;


namespace SantexAPI.Services
{
    public class ControllerDBService
    {
        private SantexDBConnection _SantexDBConnection;
        public ControllerDBService()
        {
            _SantexDBConnection = new SantexDBConnection();
        }

        public DbRawSqlQuery<DAL.Team> GetAllTeams()
        {
            var listTeams = _SantexDBConnection.Database.SqlQuery<DAL.Team>("select * from Team");
            return listTeams;
        }

        public ICollection<Player> GetPlayersByTLATeam(string tla)
        {
            //_SantexDBConnection.Configuration.ProxyCreationEnabled = false;
            Team team = _SantexDBConnection.Team.FirstOrDefault<DAL.Team>(x => x.TLA == tla);
           return team.Player;

        }

        public int CountPlayersByLeague(string code)
        {
            string sql = "Select distinct CAST(Count(*) as varchar(10)) as Players from TeamCompetition TC " +
                         " INNER JOIN Competition C ON TC.CompetitionID = C.ID " +
                         " INNER JOIN Team T ON T.ID = TC.TeamID " +
                         " INNER JOIN Player P on T.ID = P.TeamID " +
                         " where Code = @code ";

            sql = sql.Replace("@code", "'" + code + "'");

            string value = _SantexDBConnection.Database.SqlQuery<string>(sql)
                           .FirstOrDefault();

            return int.Parse(value);
        }
    }

}