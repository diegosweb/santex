﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SantexAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web

            // Rutas de API web
            config.MapHttpAttributeRoutes();

           config.Routes.MapHttpRoute(
                name: "SantexAPIImport",
                routeTemplate: "import-league/{id}",
                defaults: new { controller = "ImportLeague", action = "GetCode" }
            );

            config.Routes.MapHttpRoute(
                name: "SantexAPIView",
                routeTemplate: "total-player/{id}",
                defaults: new { controller = "TotalPlayers", action = "CountPlayers" }
            );

            config.Routes.MapHttpRoute(
                 name: "SantexAPI2",
                 routeTemplate: "{controller}/{action}/{id}",
                 defaults: new { id = RouteParameter.Optional }
             );
        }
    }
}
