﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SantexAPI.Models
{
    public class Season
    {
        public string season { get; set; }
        public int id { get; set; }
        public string startDate {get; set;}
        public string endDate { get; set; }
        public int currentMatchday { get; set; }
        public string winner { get; set; }
    }
}