﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SantexAPI.Models
{
    public class Player
    {
       public int id { get; set; }
       public string name { get; set; }
       public string position { get; set; }
       public DateTime dateOfBirth { get; set; }
       public string countryOfBirth { get; set; }
       public string nationality { get; set; }
       public string shirtNumber { get; set; }
       public string role { get; set; }
    }
}