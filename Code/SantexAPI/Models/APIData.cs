﻿using SantexAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SantexAPI.Models
{
    public class APIData
    {
        public ImportClass ImportClass { get; set; }
        public List<TeamInfo> TeamInfoList { get; set; }
        public ResultMessage ResultMessage { get; set; }
        public int CompetitionID { get; set; }

        public APIData() { }
        public APIData( ImportClass importClass, List<TeamInfo> teamInfoList, ResultMessage rm)
        {
            this.ImportClass = importClass;
            this.TeamInfoList = teamInfoList;
            this.ResultMessage = rm;

        }
    }
}