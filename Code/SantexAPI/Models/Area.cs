﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SantexAPI.Models
{
    public class Area
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}