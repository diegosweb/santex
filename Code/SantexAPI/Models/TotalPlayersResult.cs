﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SantexAPI.Models
{
    public class TotalPlayersResult
    {
        public int Total { get; set; }
        public TotalPlayersResult(int initialTotal)
        {
            this.Total = initialTotal;
        }
    }
   
}