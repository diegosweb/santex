﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SantexAPI.Models
{
    public class ImportClass
    {
        public int count { get; set; }
        //public string filters { get; set; }
        public Competition competition { get; set; }
        //public Season season { get; set; }
        public IList<Team> teams { get; set; }

    }
}