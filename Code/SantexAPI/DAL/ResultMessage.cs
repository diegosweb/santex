﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SantexAPI.DAL
{
    public class ResultMessage
    {
        public Nullable<int> Code { get; set; }
        public string Message { get; set; }
        public ResultMessage() { }

        public ResultMessage(int? code, string message)
        {
            this.Code = code;
            this.Message = message;
        }
    }
}