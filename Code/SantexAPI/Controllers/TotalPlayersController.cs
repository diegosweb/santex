﻿using System;
using System.Web.Http;
using SantexAPI.Models;
using SantexAPI.Services;

namespace SantexAPI.Controllers
{
    [RoutePrefix("TotalPlayers")]
    public class TotalPlayersController : ApiController
    {
        public TotalPlayersController()
        {
        }

        //[Route("GetAreas")]
        [Route("{code}")]
        [HttpGet]
        public IHttpActionResult CountPlayers([FromUri] string code)
        {
            #region Initialization
            ControllerDBService dbService = new ControllerDBService();
            TotalPlayersResult result = new TotalPlayersResult(0);
            int resultCode = 0;
            IHttpActionResult resultsCode = Ok();
            #endregion

            #region Data
            try
            {
                result.Total = dbService.CountPlayersByLeague(code);
                resultCode = 200;
            }
            catch(Exception ex)
            {
                resultCode = 504;
            }
            #endregion

           
            #region Codes
            switch (resultCode)
            {
                case 200:
                    if (result.Total > 0)
                        resultsCode = Ok(result);
                    else
                        resultsCode = NotFound();
                    break;

                case 504:
                    resultsCode = InternalServerError();
                    break;

            }
            #endregion

            return resultsCode;

        }
    }
}