﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using SantexAPI.DAL;
using SantexAPI.Models;
using SantexAPI.Services;

namespace SantexAPI.Controllers
{
    [RoutePrefix("ImportLeague")]
    public class ImportLeagueController : ApiController
    {
        public ImportLeagueController()
        {
        }

        //[Route("GetAreas")]
        [Route("{code}")]
        [HttpGet]
        public async Task<HttpResponseMessage>  Get([FromUri] string code)
        {
            #region Initialization
            ImportDBService dbService = new ImportDBService(code);
            APIService service = new APIService();
            ResultMessage rm = new ResultMessage();
            HttpRequestMessage request = new HttpRequestMessage();
            int httpResponseValue = 0;
            var response = request.CreateResponse();
            #endregion

            #region Data
            if (dbService.CheckLeagueExists())
            {
                rm.Code = 409;
                rm.Message = "League already imported";
            }
               
            else
            {
                APIData data = await service.GetDataFromAPI(code);
                if (data.ResultMessage.Code != 404 && data.ResultMessage.Code != 504)
                    rm = dbService.ImportIntoDB(data.ImportClass, data.TeamInfoList, data.ResultMessage);
                else
                {
                    rm.Code = data.ResultMessage.Code;
                    rm.Message = data.ResultMessage.Message;
                }
            }
            #endregion

            #region Codes
            switch (rm.Code)
            {
                case 200:
                    response = request.CreateResponse(HttpStatusCode.OK, "Succesfully imported");
                    break;

                case 409:
                    httpResponseValue = 409;
                    response = Request.CreateResponse((HttpStatusCode)httpResponseValue, rm.Message );
                    break;

                case 404:
                    response = Request.CreateResponse(HttpStatusCode.NotFound);
                    break;

                case 429:
                    httpResponseValue = 429;
                    response = Request.CreateResponse((HttpStatusCode)httpResponseValue, rm.Message);
                    break;

                case 504:
                    httpResponseValue = 504;
                    response = Request.CreateResponse((HttpStatusCode)httpResponseValue, rm.Message);
                    break;
            }
            #endregion
            return response;
            


        }


    }
}