﻿using System.Web.Http;
using SantexAPI.DAL;
using SantexAPI.Repository;
using Newtonsoft.Json;
using SantexAPI.Services;


namespace SantexAPI.Controllers
{
    public class TeamController : ApiController
    {
        private IRepository<Team> _repository = null;
        private ControllerDBService _service;
        public TeamController()
        {
            this._repository = new Repository<Team>();
            this._service = new ControllerDBService();
        }

        [ActionName("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var result = this._service.GetAllTeams();
            string jsonString = JsonConvert.SerializeObject(result);

            return Ok(result);
        }

        [ActionName("GetPlayersByTeam")]
        [HttpGet]
        public IHttpActionResult GetPlayersByTeam(string tla)
        {
           
            var result = this._service.GetPlayersByTLATeam(tla);
            string jsonString = JsonConvert.SerializeObject(result);

            return Ok(result);
        }
    }
}
