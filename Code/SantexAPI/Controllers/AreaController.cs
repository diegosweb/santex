﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using SantexAPI.DAL;
using SantexAPI.Repository;

namespace SantexAPI.Controllers
{
    public class AreaController : ApiController
        {
           private IRepository<Area> _repository = null;
            public AreaController()
            {
                this._repository = new Repository<Area>();
            } 
           
            //[Route("GetAreas")]
            [ActionName("GetAreasList")]
            [HttpGet]
            public HttpResponseMessage GetAreasList()
            {
                var result = _repository.GetAll();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }

            [ActionName("GetArea")]
            [HttpGet]
            public HttpResponseMessage GetArea(int AreaId)
            {
                var result = _repository.GetById(AreaId);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }
           
            [Route("DeleteArea")]
            [HttpGet]
            public HttpResponseMessage DeleteArea(int AreaId)
            {
                _repository.Delete(AreaId);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
           
            [Route("UpdateArea")]
            [HttpGet]
            public HttpResponseMessage UpdateArea(Area AreaDetails)
            {
                var result = _repository.Update(AreaDetails);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }  
        }
    }
