﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SantexAPI.DAL;
using SantexAPI.Repository;

namespace SantexAPI.Controllers
{
    public class PlayerController : ApiController
    {
        private IRepository<Player> _repository = null;
        public PlayerController()
        {
            this._repository = new Repository<Player>();
        }

        //[Route("GetPlayers")]
        [ActionName("GetPlayersList")]
        [HttpGet]
        public HttpResponseMessage GetPlayersList()
        {
            HttpResponseMessage response = null;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, _repository.GetAll());
            }
            catch(Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
          
            return response;
        }

        [ActionName("GetPlayer")]
        [HttpGet]
        public HttpResponseMessage GetPlayer(int playerId)
        {
            HttpResponseMessage response = null;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, _repository.GetById(playerId));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;

        }

        [Route("DeletePlayer")]
        [HttpGet]
        public HttpResponseMessage DeletePlayer(int playerId)
        {

            HttpResponseMessage response = null;
            try
            {
                _repository.Delete(playerId);
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;

        }

        [Route("UpdatePlayer")]
        [HttpPut]
        public HttpResponseMessage UpdatePlayer([FromBody] Player playerDetails)
        {
            HttpResponseMessage response = null;
            try
            {
                _repository.Update(playerDetails);
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;

        }
    }
}
